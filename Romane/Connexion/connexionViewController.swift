//
//  connexionViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 26/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import FirebaseAuth

class connexionViewController: UIViewController {
    
    //MARK - Button
    @IBOutlet weak var ibconnexion: UIButton!
    
    //MARK - TextField
    
    @IBOutlet weak var Email: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Email.delegate = self
        passwordTextfield.delegate = self
       // passwordTexfield.delegate = self
    }
    
    //MARK - Action
    
    @IBAction func connexionAction(_ sender: Any) {
        
                guard let email = Email.text, let pass = passwordTextfield.text else{return}
                if email != "" && pass != ""{
                    
                    Auth.auth().signIn(withEmail: email, password: pass){(authResult,error) in
                              
                              if error != nil {
                                  print(error.debugDescription)
                                 let alert = UIAlertController(title: "Erreur", message: "L'adresse ou le mot de passe est pas bon", preferredStyle: .alert)
                                                       alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                                                           
                                                       }))
                                                       self.present(alert, animated: true, completion: nil)
                              }else{
                                print("Connexion de \(email) réussi ✅")
        //                          if   let _vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "test") as? ConfigTabBarController{
        //                                                DispatchQueue.main.async {
        //
        //                                                    self.navigationController?.pushViewController(_vc, animated: true)
        //
        //                                                }
        //                                            }
                                
                                
                                self.performSegue(withIdentifier: "goToHomepage", sender: self)
                                
                              }
                              
                          }
                }else{
                    let alert = UIAlertController(title: "Erreur", message: "Un des champs n'est pas remplie", preferredStyle: .alert)
                                  alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                                      
                                  }))
                                  self.present(alert, animated: true, completion: nil)
                }
        
    }
    
    
    
    @IBAction func showInscription(_ sender: Any) {
   let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
   let nextViewController = storyBoard.instantiateViewController(withIdentifier: "IscriptionPage") as! inscriptionViewController
   self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    

}

