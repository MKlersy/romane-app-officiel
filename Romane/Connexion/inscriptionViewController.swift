//
//  inscriptionViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 26/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseDatabase

class inscriptionViewController: UIViewController {
        
        var dbRef: DatabaseReference!
        static let reuseIdentifier = "IscriptionPage"
        
        // MARK: - Button
        @IBOutlet weak var ibInscription: UIButton!
    
        
        // MARK: - Label
        

        
        // MARK: - TextField
        @IBOutlet weak var mailTextfield: UITextField!
        @IBOutlet weak var usernameTextfield: UITextField!
        @IBOutlet weak var nomTextField: UITextField!
        @IBOutlet weak var passwordTexfield: UITextField!
        @IBOutlet weak var confirmpasswordTextfield: UITextField!
    
        
        
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // MARK: - Delegate TextField
            mailTextfield.delegate = self
            usernameTextfield.delegate = self
            nomTextField.delegate = self
            passwordTexfield.delegate = self
            confirmpasswordTextfield.delegate = self
            
            
        }
        
        
        
        // MARK: - Action
        
        
    @IBAction func ibSignUp(_ sender: Any) {
            
            guard
                let email = mailTextfield.text,
                let password = passwordTexfield.text,
                let confirm = confirmpasswordTextfield.text,
                let username = usernameTextfield.text,
                let name = nomTextField.text
                else{return}
            
            
            
            
            if username != "" && password != "" && email != "" && confirm != "" && password == confirm && email.contains("@") && password.count >= 8{
                
                Auth.auth().createUser(withEmail: email, password: password) { (authresult, error) in
                    
                    if error != nil{
                        print(error.debugDescription)
                        
                    }else{
                              print("Connexion de \(username) réussi ✅")
                        let idUsers = Auth.auth().currentUser?.uid
                        
                        let pseudo = username + "." + name.prefix(1)
                        
                        let mesCartes = ["mesCartesInscription":false]
                        let dict = ["username": username, "nom":name, "email": email, "id":idUsers!,"pseudo":pseudo,"mesCartes":mesCartes] as [String : Any]
                        
                        self.dbRef = Database.database().reference()
                        
                        self.dbRef.child("users").child(idUsers!).setValue(dict)
                        self.performSegue(withIdentifier: "goToHomepage", sender: self)

                    }
                    
                }
                
            }else{
                
                if password != confirm{
                    
                    let alert = UIAlertController(title: "Erreur", message: "Les mots de passe ne sont pas identique", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                if email.contains("@") != true{
                    let alert = UIAlertController(title: "Erreur", message: "l'adresse mail doit avoir les forme ‘MonAdresse@fournisseur.(com/fr/eu/...)‘", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                if usernameTextfield.text == "" || passwordTexfield.text == "" || mailTextfield.text == "" || confirmpasswordTextfield.text == "" {
                    let alert = UIAlertController(title: "Erreur", message: "Un des champs est vide", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
                
                if password.count < 8 {
                    let alert = UIAlertController(title: "Erreur", message: "Le mot de passe doit faire minimum 8 caractère", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: .default, handler: { _ in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
        }
        
    
        
    @IBAction func showviewConnexion(_ sender: Any) {
      
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "connexion") as! connexionViewController
                self.present(nextViewController, animated:true, completion:nil)
        
    }
    
        
      
        
        
    }

