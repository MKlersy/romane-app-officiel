//
//  ViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 26/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit


class AppContainerViewController: UIViewController {
     
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        AppManager.shared.appContainer = self
        AppManager.shared.showApp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     hideNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     showNavigationBar()
    }

}


