//
//  AppManager.swift
//  Romane
//
//  Created by Mathieu Klersy on 26/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class AppManager {
    
    static let shared = AppManager()
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    var appContainer: AppContainerViewController!
    
    private init() { }
    
    func showApp() {
        var viewController: UIViewController
        
        if Auth.auth().currentUser == nil {
            viewController = storyboard.instantiateViewController(withIdentifier: "IscriptionPage")

        } else {
            viewController = storyboard.instantiateViewController(withIdentifier: "TabBar")
        }
        
        appContainer.present(viewController, animated: true, completion: nil)
        
    }
    
    func logout() {
        try! Auth.auth().signOut()
        appContainer.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
}
