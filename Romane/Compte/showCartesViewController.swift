//
//  showCartesViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 27/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class showCartesViewController: UIViewController, UICollectionViewDataSource {
    

    
    
    //MARK: Outlet
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var collectionViewCartes: UICollectionView!
    
    //MARK: Variables
    var dbRef: DatabaseReference!
    var stringName:String = ""
    var stringId:String = ""
    var items = [cartesData]()
    let userID = Auth.auth().currentUser?.uid
    var idChild:String = ""
    var counter:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = stringName
        
        //MARK: Chargement data
        let dbRefCartes = Database.database().reference().child("users").child(userID!).child("mesCartes").child(stringId).child("cartesData")
        dbRefCartes.observe(.value, with: { snapshot in
            var newItems: [cartesData] = []
        for itemsSnapshot in snapshot.children {
            
            let objectSnapshot = cartesData(snapshot: itemsSnapshot as! DataSnapshot)
            newItems.append(objectSnapshot)
        }
            self.items = newItems
        self.collectionViewCartes.reloadData()
        
        })
    }
    
    
    //MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = items[indexPath.row]
        let cell = collectionViewCartes.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! cartesCollectionViewCell
        
        cell.name?.text = data.name
        cell.numberCard?.text = String(data.number)
        cell.reussiCarte?.text = String(data.reussi)
        cell.echecCarte?.text = String(data.echec)
        cell.idHidden?.text = String(data.key)
        cell.idPrincipalHidden?.text = String(stringId)
    
        cell.buttonTrain.tag = indexPath.row
        return cell
    }
        
        //MARK: test segue
    @IBAction func goToTrain2(_ sender: UIButton) {
        self.idChild = items[sender.tag].key! as Any as! String
        
        self.performSegue(withIdentifier: "goToTrain", sender: self)
    }
    

    @IBAction func goToAddCartes(_ sender: UIButton) {
        self.idChild = items[sender.tag].key! as Any as! String
        self.counter = items[sender.tag].number! as Any as! Int
        print(items[sender.tag].number! as Any as! Int)
        self.performSegue(withIdentifier: "goToAddCartes", sender: self)
    }
    
    //MARK: Ajouter new deck de cartes
    
    @IBAction func addDeckCartes(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Ajouter un deck de cartes", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Sauvegarder", style: UIAlertAction.Style.default) { _ in
            
            guard let textField = alert.textFields?[0], let text = textField.text else {return}
            
            let matiereItem = ["echec": 0,"reussi":0,"number":0,"name":text] as [String : Any]
            guard let key = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").childByAutoId().key else { return }
            Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(key).updateChildValues(matiereItem)
        }
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "goToTrain" {
           if let destination = segue.destination as? mesCartesViewController {
            destination.stringId = self.stringId
            destination.objectToInject = self.idChild
           }
       }
        if segue.identifier == "goToAddCartes" {
            if let destination = segue.destination as? AddCardController {
             destination.stringId = self.stringId
             destination.counter = self.counter
             destination.objectToInject = self.idChild
            }
        }
    }

}



