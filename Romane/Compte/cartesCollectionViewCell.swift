//
//  cartesCollectionViewCell.swift
//  Romane
//
//  Created by Mathieu Klersy on 27/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class cartesCollectionViewCell: UICollectionViewCell {
    
    //MARK: Variables
    let userId = Auth.auth().currentUser?.uid
    
    //MARK: Outlet
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberCard: UILabel!
    @IBOutlet weak var reussiCarte: UILabel!
    @IBOutlet weak var echecCarte: UILabel!
    @IBOutlet weak var idHidden: UILabel!
    @IBOutlet weak var idPrincipalHidden: UILabel!
    @IBOutlet weak var buttonTrain: UIButton!
    
//    @IBAction func goToTrain(_ sender: UIButton) {
//        print(idHidden.text! as Any)
//        print(idPrincipalHidden.text! as Any)
//
//    }
    
    @IBAction func deleteDeck(_ sender: UIButton) {
        print(idPrincipalHidden.text! as Any)
        Database.database().reference().child("users").child(userId!).child("mesCartes").child(idPrincipalHidden.text!).child("cartesData").child(idHidden.text!).removeValue()
    }
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.name.text = "Chargement"
    }
}
