//
//  tableMatiereViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 27/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase
//import FirebaseAuth

class tableMatiereViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: TableView
    let ref = Database.database().reference()
    let userID = Auth.auth().currentUser?.uid
    var items: [cartes] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        let cartes = items[indexPath.row]
        
        cell.textLabel?.text = cartes.name
        return cell
    }



    override func viewDidLoad() {
        super.viewDidLoad()

        ref.child("users").child(userID!).child("mesCartes").observe(.value, with: {snapshot in
            var newItems: [cartes] = []
            
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let cartesItem = cartes(snapshot: snapshot){
                    newItems.append(cartesItem)
                }
            }
            self.items = newItems
            self.tableView.reloadData()
        })

    }
    
    //MARK: Outlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Button
    @IBAction func addButton(_ sender: AnyObject) {
        print("ici")
        let alert = UIAlertController(title: "Ajouter une matière", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Sauvegarder", style: UIAlertAction.Style.default) { _ in
            
            guard let textField = alert.textFields?[0], let text = textField.text else {return}
            
            let matiereItem = ["name": text] as [String : Any]
            guard let key = self.ref.child("users").child(self.userID!).child("mesCartes").childByAutoId().key else { return }
            self.ref.child("users").child(self.userID!).child("mesCartes").child(key).updateChildValues(matiereItem)
        }
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCartes" {
            let details = segue.destination as! showCartesViewController
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
                let entre = items[indexPath.row]

                details.stringName = entre.name
                details.stringId = entre.key
                
                print(entre.key)
            }

        }
    }
    
}
