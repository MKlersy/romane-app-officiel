//
//  MenuViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 26/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class MenuViewController: UIViewController {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var maView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        prepar_design()
        connection()
        
    }
    
    
    
    
    
    func connection(){
        
        let ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        ref.child("users").child(userID!).observe(.value) {
            (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
          var pseudo =  value?["pseudo"] as? String ?? "Pas de pseudo"
         let message = "Bonjour , "
            self.labelUsername.text = message + pseudo + " !"
        }
    }
    
    
    
    func  prepar_design(){
        photo.layer.cornerRadius = photo.frame.width/2;
        maView.layer.cornerRadius = 20;
        maView.layer.shadowRadius = 20;
        maView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        maView.layer.shadowOffset = .zero
        maView.layer.shadowOpacity = 1
    }
    
  /*
    @IBAction func showMesCartes(_ sender: Any) {
        showMesCartes()
    }
    
    @IBAction func showMoncompte(_ sender: Any) {
        showmonCompte()
    }
    
    func showmonCompte(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "monCompte") as! monCompteViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    func showMesCartes(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "matiere") as! tableMatiereViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     hideNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     showNavigationBar()
    } 
}



