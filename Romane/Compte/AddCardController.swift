//
//  AddCardController.swift
//  Romane
//
//  Created by Mathis Higuinen on 11/07/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class AddCardController: UIViewController {
    
    var userID = Auth.auth().currentUser?.uid
    var stringName:String = ""
    var stringId:String = ""
    var objectToInject:String = ""
    
    var amountOfQuestion:Int = 0
    var dataArray = [String]()
    var actualCounter:Int = 0
    var actualCounterNon:Int = 0
    var actualCounterOui:Int = 0
    var actualRandom:Int = 0
    var counter:Int = 0

    var verif = false

//MARK: Outlet
    @IBOutlet weak var TextFieldQuestion: UITextField!
    @IBOutlet weak var btnAddCard: UIButton!
    @IBOutlet weak var TextFieldReponse: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TextFieldQuestion.delegate = self
        TextFieldQuestion.delegate = self
        
    }
    
    
    @IBAction func SaveCard(_ sender: Any) {
        addCard()
    }

    
    
    func design(){
        TextFieldReponse.layer.cornerRadius = 20
        TextFieldQuestion.layer.cornerRadius = 20
        TextFieldReponse.placeholder = "Entrez la réponse "
        TextFieldQuestion.placeholder = "Entrez la Question "
        btnAddCard.layer.cornerRadius = 15
        btnAddCard.layer.borderColor = #colorLiteral(red: 0.3326475024, green: 0.8139288425, blue: 0.9769045711, alpha: 1)
        btnAddCard.layer.shadowOpacity = 1
        btnAddCard.layer.shadowOffset = .zero
        btnAddCard.layer.shadowRadius = 15
        btnAddCard.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    func addCard(){
        guard let Question = TextFieldQuestion.text, let Reponse = TextFieldReponse.text else {return}
        
        let data = ["nom": "no name", "question": Question, "reponse": Reponse, "variable": String(self.counter) + "_false"] as [String: Any]
        guard let key = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject).child("cartes").childByAutoId().key else {return}
        Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject).child("cartes").child(key).updateChildValues(data)
                  
    }
    

//    override func viewWillAppear(_ animated: Bool) {
//     super.viewWillAppear(animated)
//     hideNavigationBar()
//    }

//    override func viewWillDisappear(_ animated: Bool) {
//     super.viewWillDisappear(animated)
//     showNavigationBar()
//    }

}
