//
//  monCompteViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 04/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import Lottie

class monCompteViewController: UIViewController {
    
    //MARK: Outlet
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var descTextField: UITextView!
    @IBOutlet weak var pseudoTextField: UITextField!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lottieView: UIView!
    
    //MARK: Variables
    var uid = Auth.auth().currentUser?.uid
    var animationView = AnimationView(name:"checkMark")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let ref = Database.database().reference().child("users").child(uid!)
        
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            let data = snapshot.value as? NSDictionary
            self.emailTextField.text = data?["email"] as? String ?? "Pas d'email"
            
            self.pseudoTextField.text = data?["username"] as? String ?? "Pas de pseudo"
            
            self.descTextField.text = data?["desc"] as? String ?? "Pas encore de description"
            
            self.image.sd_setImage(with: URL(string:data?["urlImage"] as? String ?? "https://cours-informatique-gratuit.fr/wp-content/uploads/2014/05/compte-utilisateur-1.png"))
        })
        
    }
    
    @IBAction func majButton(_ sender: UIButton) {
        
        let refMaj = Database.database().reference(withPath: "users").child(uid!)
        
        let dict = ["username": self.pseudoTextField.text, "email": self.emailTextField.text, "desc": self.descTextField.text] as [String: Any]
        
        refMaj.updateChildValues(dict)
        
        lottieView.isHidden = false
        animationView.frame.size = CGSize(width: 200, height: 200)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .playOnce
        lottieView.addSubview(animationView)
        animationView.play()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.lottieView.isHidden = true
        }
    }
    
    @IBAction func logOutButton(_ sender: UIButton) {
        
        let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
            self.performSegue(withIdentifier: "logOutSegue", sender: self) 
        } catch let signOutError as NSError {
          print ("Erreur dans la déconnexion: %@", signOutError)
        }
        
    }
    
    @IBAction func modifierphoto(_ sender: Any) {
        
    }
    
    
    
}

