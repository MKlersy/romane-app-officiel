//
//  firebase.swift
//  Romane
//
//  Created by Mathieu Klersy on 27/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import Foundation
import Firebase

struct cartes {
    let ref: DatabaseReference?
    let key: String
    let name: String
    
    init(name:String, key:String=""){
        self.ref = nil
        self.key = key
        self.name = name
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String
            else {
                return nil
            }
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = name
    }
    func toAnyObject() -> Any {
        return [
            "name" : name
        ]
    }
}

struct users {
    let ref: DatabaseReference?
    let key: String
    let pseudo: String
    let email: String
    let id: String
    
    init(pseudo:String, key:String="", email:String, id:String){
        self.ref = nil
        self.key = key
        self.pseudo = pseudo
        self.email = email
        self.id = id
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let pseudo = value["pseudo"] as? String,
            let email = value["email"] as? String,
            let id = value["id"] as? String
            else {
                return nil
            }
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.pseudo = pseudo
        self.email = email
        self.id = id
    }
    func toAnyObject() -> Any {
        return [
            "pseudo" : pseudo,
            "email" : email,
            "id" : id
        ]
    }
}

struct cartesData {
    let key:String!
    let name:String!
    let reussi:Int!
    let echec:Int!
    let number:Int!
    
    let itemRef: DatabaseReference?
    
    init(key:String,name:String,reussi:Int,echec:Int,number:Int) {
        self.key = key
        self.name = name
        self.reussi = reussi
        self.echec = echec
        self.number = number
        self.itemRef = nil
    }
    
    init (snapshot:DataSnapshot) {
        key = snapshot.key
        itemRef = snapshot.ref
        
        let snapshotValue = snapshot.value as? NSDictionary
        
        if let nom = snapshotValue?["name"] as? String {
            name = nom
        } else {
            name = ""
        }
        
        if let reussite = snapshotValue?["reussi"] as? Int {
            reussi = reussite
        } else {
            reussi = 0
        }
        
        if let echou = snapshotValue?["echec"] as? Int {
            echec = echou
        } else {
            echec = 0
        }
        
        if let nombre = snapshotValue?["number"] as? Int {
            number = nombre
        } else {
            number = 0
        }
    }
}

class cartesPreview {
    var name: String?
    var reussi: Int?
    var echec: Int?
    var nombre: Int?
    var id:Int?
    
    init (id:Int, dictionary:[String: AnyObject]) {
        self.id = id
        if let name = dictionary["name"] as? String {
            self.name = name
        }
        
        if let reussi = dictionary["reussi"] as? Int {
            self.reussi = reussi
        }
        
        if let echec = dictionary["echec"] as? Int {
            self.echec = echec
        }
        
        if let nombre = dictionary["nombre"] as? Int {
            self.nombre = nombre
        }
    }
}


struct sousCartesData {
    let key:String!
    let name:String!
    let reussi:Bool!
    let question:String!
    let reponse:String!
    
    let itemRef: DatabaseReference?
    
    init(key:String,name:String,reussi:Bool,question:String,reponse:String) {
        self.key = key
        self.name = name
        self.reussi = reussi
        self.question = question
        self.reponse = reponse
        self.itemRef = nil
    }
    
    init (snapshot:DataSnapshot) {
        key = snapshot.key
        itemRef = snapshot.ref
        
        let snapshotValue = snapshot.value as? NSDictionary
        
        if let nom = snapshotValue?["name"] as? String {
            name = nom
        } else {
            name = ""
        }
        
        if let reussite = snapshotValue?["reussi"] as? Bool {
            reussi = reussite
        } else {
            reussi = false
        }
        
        if let questionne = snapshotValue?["question"] as? String {
            question = questionne
        } else {
            question = ""
        }
        
        if let reponsese = snapshotValue?["reponse"] as? String {
            reponse = reponsese
        } else {
            reponse = ""
        }
    }
}
