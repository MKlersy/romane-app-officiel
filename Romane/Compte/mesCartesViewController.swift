//
//  mesCartesViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 28/05/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase
import Lottie

class mesCartesViewController: UIViewController {

    //MARK: Variables
    var stringName:String = ""
    var stringId:String = ""
    var objectToInject:String = ""
    var userID = Auth.auth().currentUser?.uid
    var amountOfQuestion:Int = 0
    var dataArray = [String]()
    var actualCounter:Int = 0
    var actualCounterNon:Int = 0
    var actualCounterOui:Int = 0
    var actualRandom:Int = 0
    var animationView = AnimationView(name:"checkMark")
    var verif = false
    var rotate = false
    
    //MARK: Outlet
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelQuestion: UILabel!
    @IBOutlet weak var labelReponse: UILabel!
    @IBOutlet weak var labelId: UILabel!
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var buttonNon: UIButton!
    @IBOutlet weak var buttonStop: UIButton!
    @IBOutlet weak var buttonOui: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(objectToInject)
        print(stringId)
        labelName.text = objectToInject
        secondView.isHidden = true
        
        
        let ref = Database.database().reference().child("users").child(userID!).child("mesCartes").child(stringId).child("cartesData").child(objectToInject)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            let data = snapshot.value as? NSDictionary
            self.actualCounter = data?["number"] as? Int ?? 1
            self.actualCounterNon = data?["echec"] as? Int ?? 1
            self.actualCounterOui = data?["reussi"] as? Int ?? 1
            self.actualCounter -= 1
            self.reloadData()
        })
        
            
    }

    @IBAction func swipView(_ sender: UISwipeGestureRecognizer) {
  
    }
    
    func reloadData() {
        print(String(self.actualCounter) + " test nb counter")
        let ref = Database.database().reference().child("users").child(userID!).child("mesCartes").child(stringId).child("cartesData").child(objectToInject)
        self.dataArray.removeAll()
        ref.child("cartes").queryOrdered(byChild: "variable").queryEqual(toValue: String(self.actualCounter)+"_false").observeSingleEvent(of: .value, with: {snapshot in
            self.amountOfQuestion = Int(snapshot.childrenCount)
            for child in snapshot.children {
                let data = child as! DataSnapshot
                self.dataArray.append(data.key)
            }
            print(self.dataArray)
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
            if(self.amountOfQuestion>0){
                let randomNumber = Int(arc4random_uniform(UInt32(Int(self.amountOfQuestion)))) + 1
                self.actualRandom = randomNumber
                let ref = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject).child("cartes")
                ref.child(self.dataArray[Int(randomNumber - 1)]).observeSingleEvent(of: .value, with: {snapshot in
                    let data = snapshot.value as? NSDictionary

                    self.labelQuestion.text = data?["question"] as? String ?? "Pas de question"
                    self.labelReponse.text = data?["reponse"] as? String ?? "Pas de question"
                })
                
            } else {
                if(self.verif != false) {
                    self.lottieView.isHidden = false
                    self.animationView.frame.size = CGSize(width: 200, height: 200)
                    self.animationView.contentMode = .scaleAspectFit
                    self.animationView.loopMode = .playOnce
                    self.lottieView.addSubview(self.animationView)
                    self.animationView.play()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                        self.lottieView.isHidden = true
                    }
                    
                    print("pas de question")
                    let ref = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject)
                    ref.updateChildValues(["number":self.actualCounter+1])
                    self.actualCounter += 1
                    self.reloadData()
                } else {
                    self.actualCounter += 1
                    self.verif = true
                    self.reloadData()
                }
                
                
            }
        })
        
    }
    @IBAction func addCartes(_ sender: UIButton) {
  
        let alert = UIAlertController(title: "Ajouter une carte au deck", message: "", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Sauvegarder", style: UIAlertAction.Style.default) { _ in
            guard let textField = alert.textFields?[0], let text = textField.text else {return}
            guard let textField2 = alert.textFields?[1], let text2 = textField2.text else {return}
            guard let textField3 = alert.textFields?[2], let text3 = textField3.text else {return}
            
            let data = ["nom": text, "question": text2, "reponse": text3, "variable": String(self.actualCounter)+"_false"] as [String: Any]
            guard let key = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject).child("cartes").childByAutoId().key else {return}
            Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject).child("cartes").child(key).updateChildValues(data)
            
            let ref = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject)
            self.dataArray.removeAll()
            ref.child("cartes").queryOrdered(byChild: "variable").queryEqual(toValue: String(self.actualCounter)+"_false").observeSingleEvent(of: .value, with: {snapshot in
                self.amountOfQuestion = Int(snapshot.childrenCount)
                for child in snapshot.children {
                    let data = child as! DataSnapshot
                    self.dataArray.append(data.key)
                }
                print(self.dataArray)
            })
        }
        
        
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel)
        alert.addTextField { (textField) in
            textField.placeholder = "Nom"
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Question"
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Réponse"
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated:true, completion:nil)

    }
    
    
    //MARK: Fonction choix
    
    
    @IBAction func nonCallToAction(_ sender: UIButton) {
        buttonOui.isEnabled = false
        buttonNon.isEnabled = false
        buttonStop.isEnabled = false
        non()
        buttonOui.isEnabled = true
        buttonNon.isEnabled = true
        buttonStop.isEnabled = true
    }
    @IBAction func maybeCallToAction(_ sender: UIButton) {
        buttonOui.isEnabled = false
        buttonNon.isEnabled = false
        buttonStop.isEnabled = false
        non()
        buttonOui.isEnabled = true
        buttonNon.isEnabled = true
        buttonStop.isEnabled = true
    }
    @IBAction func ouiCallToAction(_ sender: UIButton) {
        buttonOui.isEnabled = false
        buttonNon.isEnabled = false
        buttonStop.isEnabled = false
        oui()
        buttonOui.isEnabled = true
        buttonNon.isEnabled = true
        buttonStop.isEnabled = true
    }
    @IBAction func changeView(_ sender: UISegmentedControl) {
        flip()
    }
    
    func non() {
        print("non")
        self.actualCounterNon += 1
        let ref = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject)
        ref.updateChildValues(["echec":self.actualCounterNon])
        
        reloadData()
    }
    
    func oui() {
        print("oui")
        
        self.actualCounterOui += 1
        let ref2 = Database.database().reference().child("users").child(self.userID!).child("mesCartes").child(self.stringId).child("cartesData").child(self.objectToInject)
        ref2.updateChildValues(["reussi":self.actualCounterOui])
        
        let ref = Database.database().reference().child("users").child(userID!).child("mesCartes").child(stringId).child("cartesData").child(objectToInject).child("cartes")
        ref.child(self.dataArray[Int(self.actualRandom - 1)]).updateChildValues(["variable":String(self.actualCounter+1)+"_false"])
        
        reloadData()
    }
    
    @objc func flip() {
        let transitionOptions: UIView.AnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        let transitionOptions2: UIView.AnimationOptions = [.transitionFlipFromLeft, .showHideTransitionViews]
        
        if(rotate == false){
            UIView.transition(with: firstView, duration: 1.0, options: transitionOptions, animations: {
                self.firstView.isHidden = true
            })
            
            UIView.transition(with: secondView, duration: 1.0, options: transitionOptions, animations: {
                self.secondView.isHidden = false
            })
            self.rotate = true
        } else {
            UIView.transition(with: secondView, duration: 1.0, options: transitionOptions2, animations: {
                self.secondView.isHidden = true
            })
            UIView.transition(with: firstView, duration: 1.0, options: transitionOptions2, animations: {
                self.firstView.isHidden = false
            })
            self.rotate = false
        }
    }
    
    
}
