//
//  extention.swift
//  Romane
//
//  Created by Mathis Higuinen on 12/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func     hideNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavigationBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
  
}

extension UIViewController: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
