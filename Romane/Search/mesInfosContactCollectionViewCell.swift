//
//  mesInfosContactCollectionViewCell.swift
//  Romane
//
//  Created by Mathieu Klersy on 05/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class mesInfosContactCollectionViewCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonObtenir: UIButton!
    
    
}
