//
//  searchViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 04/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class searchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    var items: [users] = []
    var ref = Database.database().reference()
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! searchTableViewCell
        let cartes = items[indexPath.row]
        
        cell.pseudoLabel?.text = cartes.pseudo
        cell.emailLabel?.text = cartes.email
        cell.idLabel?.text = cartes.id
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        ref.child("users").observe(.value, with: {snapshot in
            var newItems: [users] = []
            
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let cartesItem = users(snapshot: snapshot){
                    newItems.append(cartesItem)
                }
            }
            self.items = newItems
            self.tableView.reloadData()
        })
    }
    override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(animated)
     hideNavigationBar()
    }

    override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     showNavigationBar()
    } 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInfoContact" {
            let details = segue.destination as! infoContactViewController
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                let entre = items[indexPath.row]
                details.id = entre.id
            }
        }
    }
}


