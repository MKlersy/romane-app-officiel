//
//  mesInfosContactViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 05/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase
import Lottie

class mesInfosContactViewController: UIViewController,UICollectionViewDataSource {

    //MARK: Outlet
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var alreadyView: UIView!
    
    
    //MARK: Variables
    var uid:String = ""
    var idCartes:String = ""
    var dbRef: DatabaseReference!
    let userId = Auth.auth().currentUser?.uid
    var items = [cartesData]()
    var idChild:String = ""
    var value2:String = ""
    var animationView = AnimationView(name:"checkMark")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print(uid)
        print(idCartes)
        
        let ref = Database.database().reference().child("users").child(uid).child("mesCartes").child(idCartes)
        
        ref.observe(.value, with: {snapshot in
            let data = snapshot.value as? NSDictionary
            self.labelName.text = data?["name"] as? String ?? "Pas de nom"
        })
        
        ref.child("cartesData").observeSingleEvent(of: .value, with: { snapshot in
            var newItems: [cartesData] = []
            
            for itemsSnapshot in snapshot.children {
                let objectSnapshot = cartesData(snapshot: itemsSnapshot as! DataSnapshot)
                newItems.append(objectSnapshot)
            }
            self.items = newItems
            self.collectionView.reloadData()
        })
    }
    
//MARK: Collection View

    @IBAction func downloadDeck(_ sender: UIButton) {
        
        
        guard let cell = sender.superview?.superview as? mesInfosContactCollectionViewCell else {
            return // or fatalError() or whatever
        }

        let indexPath1 = collectionView.indexPath(for: cell)
        let idChild2 = items[indexPath1![1]].key! as Any as! String
        print(idChild2)
        
        let refLui = Database.database().reference().child("users").child(uid).child("mesCartes").child(idCartes).child("cartesData").child(items[indexPath1![1]].key)
        
        refLui.child("telechargement").observeSingleEvent(of: .value, with: {snapshot in
            
            if snapshot.hasChild(self.userId!) {
                self.alreadyView.isHidden = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                       self.alreadyView.isHidden = true
                   }
                
            } else {
                
                refLui.observeSingleEvent(of: .value, with: { (snapshot) in

                    let value2 = snapshot.value
                Database.database().reference().child("users").child(self.userId!).child("mesCartes").child(self.idCartes).child("cartesData").child(self.items[indexPath1![1]].key).updateChildValues(value2 as! [AnyHashable : Any])
                })
            Database.database().reference().child("users").child(self.userId!).child("mesCartes").child(self.idCartes).updateChildValues(["name":self.labelName.text as Any])
                
                Database.database().reference().child("users").child(self.userId!).child("mesCartes").child(self.idCartes).child("cartesData")
            refLui.child("telechargement").updateChildValues([self.userId!:"oui"])
                
                self.lottieView.isHidden = false
                self.animationView.frame.size = CGSize(width: 200, height: 200)
                self.animationView.contentMode = .scaleAspectFit
                self.animationView.loopMode = .playOnce
                self.lottieView.addSubview(self.animationView)
                self.animationView.play()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self.lottieView.isHidden = true
                }
            }
        })
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = items[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! mesInfosContactCollectionViewCell
        
        cell.labelName?.text = data.name
        
        return cell
    }


}
