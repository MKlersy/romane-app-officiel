//
//  infoContactViewController.swift
//  Romane
//
//  Created by Mathieu Klersy on 05/06/2020.
//  Copyright © 2020 Mathieu Klersy. All rights reserved.
//

import UIKit
import Firebase

class infoContactViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    

    //MARK: Outlets
    @IBOutlet weak var pseudoLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables
    
    var id:String = ""
    var items: [cartes] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(id)
        let ref = Database.database().reference().child("users").child(id)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            let data = snapshot.value as? NSDictionary
            self.pseudoLabel.text = data?["pseudo"] as? String ?? "Pas de pseudo"
            self.mailLabel.text = data?["email"] as? String ?? "Pas d'email"
        })
        
        ref.child("mesCartes").observe(.value, with: {snapshot in
            var newItems: [cartes] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let cartesItem = cartes(snapshot: snapshot) {
                    newItems.append(cartesItem)
                    print("oui")
                }
                
            }
            self.items = newItems
            self.tableView.reloadData()
        })
        
    }
    
    //MARK: Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath)
        let cartes = items[indexPath.row]
        
        cell.textLabel?.text = cartes.name
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToCartes" {
            
            let destination = segue.destination as! mesInfosContactViewController
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
                let entre = items[indexPath.row]
                destination.uid = self.id
                destination.idCartes = entre.key
            }

        }
    }

}
